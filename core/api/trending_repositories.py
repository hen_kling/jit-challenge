from typing import List

from flask import Blueprint, jsonify

from core.bl.base_repository_handler import BaseRepositoryHandler
from core.bl.repository_handler import RepositoryHandler
from core.dataclasses.repository import Repository
from core.bl.risk_calculator.unused_packages_risk_calculator import UnusedPackagesRiskCalculator

trending_repositories = Blueprint('trending_repositories', __name__)


@trending_repositories.route('/trending_repositories/<int:number_of_repositories>', methods=['GET'])
def get_trending_repositories(number_of_repositories: int):
    repository_handler: BaseRepositoryHandler = RepositoryHandler(number_of_repos=number_of_repositories,
                                                                  risk_score_calc=UnusedPackagesRiskCalculator())
    repos: List[Repository] = repository_handler.get_scored_repositories()
    return jsonify(repos)
