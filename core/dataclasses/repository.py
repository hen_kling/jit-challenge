# This is the repository basic information dataClass
from abc import ABC
from dataclasses import dataclass


@dataclass
class Repository(ABC):
    name: str
    author: str
    url: str
    risk_score: int = 0
