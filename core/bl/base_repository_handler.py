from abc import ABC, abstractmethod
from typing import List

from core.dataclasses.repository import Repository
from core.bl.risk_calculator.risk_calculator import RiskCalculator


class BaseRepositoryHandler(ABC):
    def __init__(self, number_of_repos: int, risk_score_calculator: RiskCalculator):
        self.number_of_repos = number_of_repos
        self.risk_score_calc: RiskCalculator = risk_score_calculator

    @abstractmethod
    def get_scored_repositories(self) -> List[Repository]:
        pass
