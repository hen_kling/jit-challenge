from abc import ABC, abstractmethod
from typing import List

from core.dataclasses.repository import Repository


class BaseRepositoriesFetcher(ABC):
    @abstractmethod
    def fetch_repositories(self, amount: int, language: str) -> List[Repository]:
        pass
