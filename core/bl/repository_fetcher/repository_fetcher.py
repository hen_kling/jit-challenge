from typing import List

import gtrending

from core.bl.repository_fetcher.base_repository_fetcher import BaseRepositoriesFetcher
from core.dataclasses.repository import Repository


class TrendingRepositoriesFetcher(BaseRepositoriesFetcher):
    def fetch_repositories(self, amount: int, language='python') -> List[Repository]:
        """
            Fetch trending repositories on GitHub
            :param amount: anount of repositories to return
            :param language: Filtering by language, eg: python
            :return: list of trending repositories
            """
        repos: List[dict] = gtrending.fetch_repos(language=language)
        repos: List[Repository] = [Repository(name=repo["name"], author=repo["author"], url=repo["url"])
                                   for repo in repos]
        return repos[:amount]
