from typing import List

from core.bl.base_repository_handler import BaseRepositoryHandler
from core.bl.repository_fetcher.repository_fetcher import TrendingRepositoriesFetcher
from core.dataclasses.repository import Repository
from core.bl.risk_calculator.risk_calculator import RiskCalculator


class RepositoryHandler(BaseRepositoryHandler):
    def __init__(self, number_of_repos: int, risk_score_calc: RiskCalculator):
        super().__init__(number_of_repos, risk_score_calc)

    def get_scored_repositories(self) -> List[Repository]:
        trending_repositories_fetcher = TrendingRepositoriesFetcher()
        repos: List[Repository] = trending_repositories_fetcher.fetch_repositories(amount=self.number_of_repos)
        for repo in repos:
            repo.risk_score = self.risk_score_calc.calculate_risk_score(repo)
        return repos
