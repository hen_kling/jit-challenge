from abc import ABC, abstractmethod
from core.dataclasses.repository import Repository


class RiskCalculator(ABC):
    @abstractmethod
    def calculate_risk_score(self, repository: Repository) -> int:
        pass
