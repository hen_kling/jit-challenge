from random import randint

from core.dataclasses.repository import Repository
from core.bl.risk_calculator.risk_calculator import RiskCalculator


class UnusedPackagesRiskCalculator(RiskCalculator):
    def calculate_risk_score(self, repository: Repository) -> int:
        return randint(0, 100)
