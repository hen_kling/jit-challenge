from typing import List

from core.bl.base_repository_handler import BaseRepositoryHandler
from core.bl.repository_handler import RepositoryHandler
from core.dataclasses.repository import Repository
from core.bl.risk_calculator.unused_packages_risk_calculator import UnusedPackagesRiskCalculator


def get_user_input():
    user_input = input("Welcome To Github Trending Repository CLI!\n"
                       "Please Enter the number of repositories you with to explore: ")
    keep_getting_input = True
    while keep_getting_input:
        try:
            number_of_repos = int(user_input)
            if number_of_repos < 0:
                raise ValueError
            keep_getting_input = False
        except ValueError:
            user_input = input("Wrong input, please enter a valid integer > 0: ")
    return number_of_repos


def display_repositories(repositories: List[Repository]):
    for repository in repositories:
        print(repository)


if __name__ == '__main__':
    number_of_repositories = get_user_input()
    repository_handler: BaseRepositoryHandler = RepositoryHandler(number_of_repos=number_of_repositories,
                                                                  risk_score_calc=UnusedPackagesRiskCalculator())
    repositories: List[Repository] = repository_handler.get_scored_repositories()
    display_repositories(repositories)
