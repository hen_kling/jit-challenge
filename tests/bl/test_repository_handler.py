from typing import List
from unittest import TestCase
from unittest.mock import MagicMock, patch, create_autospec

from core.bl.repository_handler import RepositoryHandler
from core.bl.risk_calculator.unused_packages_risk_calculator import UnusedPackagesRiskCalculator
from core.dataclasses.repository import Repository


class TestRepositoryHandler(TestCase):
    def setUp(self):
        self.mock_calculator = create_autospec(UnusedPackagesRiskCalculator)
        self.mock_calculator.calculate_risk_score.side_effect = [5, 10]
        self.repository_handler = RepositoryHandler(2, self.mock_calculator)
        self.mock_repositories = [Repository(name='repo1', author='hen kling', url='mock-url1'),
                                  Repository(name='repo2', author='hen kling', url='mock-url2')]

    @patch('core.bl.repository_handler.TrendingRepositoriesFetcher.fetch_repositories')
    def test_get_scored_repositories(self, mock_fetch_repositories: MagicMock):
        # Arrange
        mock_fetch_repositories.return_value = self.mock_repositories
        requested_repositories_number = 2

        # Act
        response: List[Repository] = self.repository_handler.get_scored_repositories()

        # Assert
        self.assertEqual(len(response), requested_repositories_number)
        self.assertEqual(response[0].risk_score, 5)
        self.assertEqual(response[1].risk_score, 10)
