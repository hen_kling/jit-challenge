from typing import List
from unittest import TestCase
from unittest.mock import patch, MagicMock

from core.bl.repository_fetcher.repository_fetcher import TrendingRepositoriesFetcher
from core.dataclasses.repository import Repository


class TestTrendingRepositoriesFetcher(TestCase):
    def setUp(self):
        self.trending_repositories_fetcher = TrendingRepositoriesFetcher()
        self.mock_fetched_repositories = [{'name': 'repo1', 'author': 'hen kling',
                                           'url': 'mock/repo/1'},
                                          {'name': 'repo2', 'author': 'hen kling',
                                           'url': 'mock/repo/2'},
                                          {'name': 'repo3', 'author': 'hen kling',
                                           'url': 'mock/repo/3'}
                                          ]

    @patch('core.bl.repository_fetcher.repository_fetcher.gtrending.fetch_repos')
    def test_fetch_repositories(self, mock_fetch_repos: MagicMock):
        # Arrange
        mock_fetch_repos.return_value = self.mock_fetched_repositories
        requested_repositories_number = 1

        # Act
        response: List[Repository] = self.trending_repositories_fetcher.fetch_repositories(
            requested_repositories_number)

        # Assert
        self.assertEqual(len(response), requested_repositories_number)
        self.assertIsInstance(response[0], Repository)
        self.assertEqual(response[0].name, self.mock_fetched_repositories[0]['name'])
        self.assertEqual(response[0].url, self.mock_fetched_repositories[0]['url'])
        self.assertEqual(response[0].author, self.mock_fetched_repositories[0]['author'])

    @patch('core.bl.repository_fetcher.repository_fetcher.gtrending.fetch_repos')
    def test_fetch_repositories_wanted_amount_exceeds_returned_repositories(self, mock_fetch_repos: MagicMock):
        # Arrange
        mock_fetch_repos.return_value = self.mock_fetched_repositories
        requested_repositories_number = 5

        # Act
        response: List[Repository] = self.trending_repositories_fetcher.fetch_repositories(
            requested_repositories_number)

        # Assert
        self.assertEqual(len(response), len(self.mock_fetched_repositories))
        for i in range(len(self.mock_fetched_repositories)):
            self.assertIsInstance(response[i], Repository)
            self.assertEqual(response[i].name, self.mock_fetched_repositories[i]['name'])
            self.assertEqual(response[i].url, self.mock_fetched_repositories[i]['url'])
            self.assertEqual(response[i].author, self.mock_fetched_repositories[i]['author'])

    @patch('core.bl.repository_fetcher.repository_fetcher.gtrending.fetch_repos')
    def test_fetch_repositories_no_repositories_found(self, mock_fetch_repos: MagicMock):
        # Arrange
        mock_fetch_repos.return_value = []
        requested_repositories_number = 3

        # Act
        response: List[Repository] = self.trending_repositories_fetcher.fetch_repositories(
            requested_repositories_number)

        # Assert
        self.assertEqual(len(response), 0)
