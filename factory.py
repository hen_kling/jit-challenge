from flask import Flask

from core.api.trending_repositories import trending_repositories


def create_app():
    app = Flask(__name__)
    app.register_blueprint(trending_repositories)

    return app
